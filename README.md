# Install OS
- burn RaspiOS Buster image on a SD card (2020-05-27-raspios-buster-lite-armhf)
- add an empty ssh file on boot partition
- add a wpa_supplicant.conf containing the wireless SSID and password

# Configure Raspberry PI
- run 'sudo raspi-config' to:
    - configure hostname (Network Options/Hostname)
    - configure timezone (Localisation Options/Timezone)
- curl -L https://bitbucket.org/lirimia/wifi_bridge/raw/master/setup.sh | bash | tee -a /home/pi/install.log

# Credits
- https://raspberrypi.stackexchange.com/questions/88954/workaround-for-a-wifi-bridge-on-a-raspberry-pi-with-proxy-arp
- https://raspberrypi.stackexchange.com/questions/108592/use-systemd-networkd-for-general-networking