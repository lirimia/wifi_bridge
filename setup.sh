#!/bin/bash

# update system
sudo apt-get update

# install drivers for WiFi dongle; the driver version should match the output from 'uname -a' command
wget http://downloads.fars-robotics.net/wifi-drivers/8188eu-drivers/8188eu-4.19.118-1311.tar.gz
tar -xzvf 8188eu-4.19.118-1311.tar.gz
./install.sh

# deinstall classic networking
sudo apt-get autoremove --purge -y ifupdown dhcpcd5 isc-dhcp-client isc-dhcp-common rsyslog
sudo apt-mark hold ifupdown dhcpcd5 isc-dhcp-client isc-dhcp-common rsyslog raspberrypi-net-mods openresolv
sudo rm -r /etc/network /etc/dhcp

# setup/enable systemd-resolved and systemd-networkd
sudo apt-get autoremove --purge -y avahi-daemon
sudo apt-mark hold avahi-daemon libnss-mdns
sudo apt-get install -y libnss-resolve
sudo ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
sudo systemctl enable systemd-networkd.service systemd-resolved.service

# configure wpa_supplicant
sudo cp /etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant-wlan0.conf
sudo systemctl disable wpa_supplicant.service
sudo systemctl enable wpa_supplicant@wlan0.service

# configure wlan0 interface
sudo curl -L https://bitbucket.org/lirimia/wifi_bridge/raw/master/08-wlan0.network -o /etc/systemd/network/08-wlan0.network

echo 'Done!'
sudo reboot





# install helpers
sudo apt-get install -y parprouted dhcp-helper
sudo systemctl stop dhcp-helper
sudo systemctl enable dhcp-helper

    # deinstall classic networking
    #sudo apt-get autoremove --purge -y avahi-daemon
    sudo apt-get install -y libnss-resolve
    sudo systemctl enable systemd-resolved.service
    sudo ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf

    # enable systemd-networkd
    sudo systemctl enable systemd-networkd.service





# enable DHCP relay
sudo sed -i 's/DHCPHELPER_OPTS="-b eth0"/# relay dhcp requests as broadcast to wlan0/' /etc/default/dhcp-helper
echo 'DHCPHELPER_OPTS="-b wlan0"' | sudo tee -a /etc/default/dhcp-helper

# configure parprouted service
sudo curl -L https://bitbucket.org/lirimia/wifi_bridge/raw/master/parprouted.service -o /etc/systemd/system/parprouted.service
sudo systemctl enable parprouted.service

# reboot system
#sudo reboot